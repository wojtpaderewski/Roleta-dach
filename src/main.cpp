#include <Arduino.h>
#include <Wire.h>
#include "PhysicalIO.hpp"
#include "ManualControl.hpp"
#include "ServoDriver.hpp"

constexpr uint32_t serialSpeed = 9600;

ServoDriver servo;
PhysicalIO physicalIO;
ManualControl manualControl;

void setup() {
    Serial.begin(serialSpeed);
    while(!Serial) {
        continue;
    }

    Serial.println(F("Initializing peripherals..."));

    physicalIO.init();
    Serial.println(F("IOs initialized!"));

    servo.init(&physicalIO);
    Serial.println(F("Servo initialized!"));

    manualControl.init(&physicalIO, &servo);
    Serial.println(F("Manual control initialized!"));
}

void loop() {
    manualControl.update();
    physicalIO.update();
}
