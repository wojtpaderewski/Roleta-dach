#ifndef __SERVODRIVER_HPP__
#define __SERVODRIVER_HPP__

#include <stdint.h>
#include <PhysicalIO.hpp>
#include <Servo.h>

constexpr uint8_t pwmForward = 62;
constexpr uint8_t pwmBackward = 112;
constexpr uint8_t pwmStop = 82;

enum Movement {
    FORWARD,
    STOP,
    BACKWARD
};

class ServoDriver {
    private:
        Servo servo;
        PhysicalIO* physicalIO;

    public:

        void init(PhysicalIO* physicalIO) {
            this->physicalIO = physicalIO;
            this->servo.attach(this->physicalIO->servoPin);
        }

        void move(Movement movement) {
            switch (movement) {
                case FORWARD:
                    this->servo.write(pwmForward);
                break;
                case BACKWARD:
                    this->servo.write(pwmBackward);
                break;
                case STOP:
                    this->servo.write(pwmStop);
                break;
            } 
        }
};  

#endif