#ifndef __PHYSICAL_IO__
#define __PHYSICAL_IO__

#include <Arduino.h>

/* Inputs */
constexpr uint8_t moveDownPin = 16;
constexpr uint8_t moveUpPin = 5;

/* PWMs */
constexpr uint8_t servoPwmPin = 2;

class PhysicalIO {
    public:
        bool moveDownButton = false;
        bool moveUpButton = false;

        uint8_t servoPin = servoPwmPin;

    void init () {
        pinMode(moveDownPin, INPUT);
        pinMode(moveUpPin, INPUT);
        this->update(); 
    }

    void update () {
        this->moveDownButton = !digitalRead(moveDownPin);
        this->moveUpButton = !digitalRead(moveUpPin);
    }
};

#endif