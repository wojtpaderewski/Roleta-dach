#ifndef __ESP_HPP__
#define __ESP_HPP__

#include <Arduino.h>
#include <string.h>
#include <ESP8266WiFi.h>
#include "ESPAsyncWebServer.h"

class Server {
    private:
        AsyncWebServer wifiServer;
        const char* ssid;
        const char* password;
        const char* serverAddress;
        uint16_t serverPort;
        char buffer[512 + 128]; // Size of Api.hpp buffer + 128 bytes for HTTP GET/POST header 
        char itoaBuffer[8];
        String header;
    
    public: 
        bool init(HardwareSerial* serial, const char* ssid, const char* password, uint16_t port) {
            this->ssid = ssid;
            this->password = password;
            this->serverPort = port;

            this->wifiServer = AsyncWebServer(port);
            if(WiFi.status() == WL_NO_SHIELD) {
                return false;
            }
            return true;
        }

        int joinAP() {
            int espStatus = WiFi.begin(this->ssid, this->password);
            return espStatus;
        }

        bool isConnectedToAP() {
            return (WiFi.status() == WL_CONNECTED);
        }

        bool establishConnectionToServer() {
            return this->client.connect(this->serverAddress, this->serverPort);
        }

        bool isConnectedToServer() {
            return this->client.connected();
        }

        void closeConnectionToServer() {
            this->client.stop();
        }
       
        void update() {
            while(!this->isConnectedToAP()) {
                Serial.println(F("Connection to AP has been lost, reconnecting..."));
                this->joinAP();
            }
            while(!this->isConnectedToServer()) {
                Serial.println(F("Connection to server has been lost, reconnecting..."));
                this->establishConnectionToServer();
            }
        }

        void makePostRequestToServer(const char* payload, const char* endpoint) {    
            strcpy(this->buffer, "POST ");
            strcat(this->buffer, endpoint);
            strcat(this->buffer, " HTTP/1.1\r\nHost: ");    
            strcat(this->buffer, this->serverAddress);
            strcat(this->buffer, ":");
            itoa(this->serverPort, this->itoaBuffer, 10);
            strcat(this->buffer, this->itoaBuffer);
            strcat(this->buffer, "\r\nContent-Length: ");
            itoa(strlen(payload), this->itoaBuffer, 10);
            strcat(this->buffer, this->itoaBuffer);
            strcat(this->buffer, "\r\nContent-Type: application/json\r\n\r\n");  // Double CRLF to mark end of the POST request header
            /* Now append body */
            strcat(this->buffer, payload);
            strcat(this->buffer, "\r\n");

            /* Send whole buffer at once */
            this->client.print(this->buffer);
        }

        void makeGetRequestToServer(char* payloadBuffer, const char* endpoint) {
            strcpy(this->buffer, "GET ");
            strcat(this->buffer, endpoint);
            strcat(this->buffer, " HTTP/1.1\r\nHost: ");      
            strcat(this->buffer, this->serverAddress);
            strcat(this->buffer, ":");
            itoa(this->serverPort, this->itoaBuffer, 10);
            strcat(this->buffer, this->itoaBuffer);
            strcat(this->buffer, "\r\n\r\n"); // Double CRLF to mark end of the GET request header
            this->client.print(this->buffer);

            constexpr uint32_t responseTimeout = 2000; //ms
            uint32_t beginTime = millis();
            uint16_t position = 0;
            while(position == 0) {
                if(responseTimeout < (millis() - beginTime)) {
                    Serial.println(F("Timeout waiting for data from ESP!"));
                    return;
                }
                while(this->client.available()) {
                    payloadBuffer[position] = this->client.read();
                    position++;
                }
                payloadBuffer[position] = '\0';
            }       
        }
        /* RSSI - Received Signal Strength Indicator */
        int32_t getCurrentRSSI() {
            return WiFi.RSSI();
        }
};

#endif