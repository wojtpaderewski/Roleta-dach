#ifndef __MANUALCONTROL_HPP__
#define __MANUALCONTROL_HPP__

#include "PhysicalIO.hpp"
#include "ServoDriver.hpp"

class ManualControl {
    private:
        PhysicalIO* physicalIO;
        ServoDriver* servo;

    public:
        void init(PhysicalIO* physicalIO, ServoDriver* servo) {
            this->physicalIO = physicalIO;
            this->servo = servo;
        }

        void update() {
            if (this->physicalIO->moveDownButton) {
                this->servo->move(BACKWARD);
            } else if (this->physicalIO->moveUpButton) {
                this->servo->move(FORWARD);
            } else {
                this->servo->move(STOP);
            }
        }
};

#endif